package course.examples.simplecalculate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{
    Button one, two, three, four, five, six, seven, eight, nine, zero;
    Button plus, minus, divide, multiply, ac, equal, dot;
    TextView display;
    float value1, value2, temp;
    boolean is_minus, is_plus, is_divide, is_multiply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);
        seven = (Button) findViewById(R.id.seven);
        eight = (Button) findViewById(R.id.eight);
        nine = (Button) findViewById(R.id.nine);
        zero = (Button) findViewById(R.id.zero);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        divide = (Button) findViewById(R.id.divide);
        multiply = (Button) findViewById(R.id.multiply);

        ac = (Button) findViewById(R.id.ac);
        equal = (Button) findViewById(R.id.equal);
        dot = (Button) findViewById(R.id.dot);

        display = (TextView) findViewById(R.id.display);


        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(display.getText().toString().equals("0"))
                {
                    display.setText("1");
                }else {
                    display.setText(display.getText() + "1");
                }
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("2");
                }else {
                    display.setText(display.getText() + "2");
                }
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("3");
                }else {
                    display.setText(display.getText() + "3");
                }
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("4");
                }else {
                    display.setText(display.getText() + "4");
                }
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("5");
                }else {
                    display.setText(display.getText() + "5");
                }
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("6");
                }else {
                    display.setText(display.getText() + "6");
                }
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("7");
                }else {
                    display.setText(display.getText() + "7");
                }
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("8");
                }else {
                    display.setText(display.getText() + "8");
                }
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("9");
                }else {
                    display.setText(display.getText() + "9");
                }
            }
        });

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(display.getText().toString().equals("0"))
                {
                    display.setText("0");
                }else {
                    display.setText(display.getText() + "0");
                }
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (display == null) {
                    display.setText("");
                } else {
                    value1 = Float.parseFloat(display.getText() + "");
                    is_plus = true;
                    display.setText(null);
                }
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (display == null) {
                    display.setText("");
                } else {
                    value1 = Float.parseFloat(display.getText() + "");
                    is_minus = true;
                    display.setText(null);
                }
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (display == null) {
                    display.setText("");
                } else {
                    value1 = Float.parseFloat(display.getText() + "");
                    is_divide = true;
                    display.setText(null);
                }
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (display == null) {
                    display.setText("");
                } else {
                    value1 = Float.parseFloat(display.getText() + "");
                    is_multiply = true;
                    display.setText(null);
                }
            }
        });

        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value2 = Float.parseFloat(display.getText() + "");

                if (is_plus) {
                    temp = value1 + value2;
                    display.setText(temp + "");
                    is_plus = false;
                }

                if (is_minus) {
                    temp = value1 - value2;
                    display.setText(temp + "");
                    is_minus = false;
                }

                if (is_divide) {
                    temp = value1 / value2;
                    display.setText(temp + "");
                    is_divide = false;
                }

                if (is_multiply) {
                    temp = value1 * value2;
                    display.setText(temp + "");
                    is_divide = false;
                }
            }
        });

        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText("0");
            }
        });

        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(display.getText() + ".");
            }
        });
    }
}